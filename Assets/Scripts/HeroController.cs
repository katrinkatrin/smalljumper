﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
	//public Rigidbody2D rigidBody;

    [SerializeField]
    //[HideInInspector]
    private int speedy = 8;

    [SerializeField]
    //[HideInInspector]
    private int speedx = 1;

    void Start()
    {
       // rigidBody = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(0, 0, 0);
    }

    void Update()
    {
        //rigidBody.velocity = new Vector2(3, rigidBody.velocity.y);
        /*
        if (Input.GetMouseButtonDown(0))
        {
        	rigidBody.velocity = new Vector2(rigidBody.velocity.x, 7);
        }
        */
       

        float horizontInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        //bool a_button = Input.GetKey("up");

        //if(a_button)
        //{
            //print("up arrow key is held down");
        //    Destroy(this.gameObject);
        //}

        //transform.Translate(Vector3.right * Time.deltaTime * speed/2 * horizontInput); 
        transform.Translate(Vector3.right * Time.deltaTime * speedx * horizontInput);
        transform.Translate(Vector3.up * Time.deltaTime * speedy * verticalInput); 

        if (transform.position.y > 4.6f)
        {
            transform.position = new Vector3(transform.position.x, 4.6f, 0);
        }
        else if (transform.position.y < -5.48f)
        {
            transform.position = new Vector3(-4.8f, -2.97f, 0);
        }

        if (transform.position.x > 6.49f)
        {
            transform.position = new Vector3(-6.49f, transform.position.y, 0);
        }
        else if (transform.position.x < -6.71f)
        {
            transform.position = new Vector3(6.71f, transform.position.y, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "coin1")
        {
            //Destroy(this.gameObject, .5f);
            Destroy(col.gameObject);
            //Destroy(gameObject);
        }

        if (col.gameObject.name == "coin2")
        {
            //Destroy(this.gameObject, .5f);
            Destroy(col.gameObject);
            //Destroy(gameObject);
        }

        if (col.gameObject.name == "coin3")
        {
            //Destroy(this.gameObject, .5f);
            Destroy(col.gameObject);
            //Destroy(gameObject);
        }
    }
}
